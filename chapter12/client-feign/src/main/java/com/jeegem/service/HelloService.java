package com.jeegem.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
/*
 * @FeignClient(value="SERVICE-PROVIDER")
 * 这里同样需要大写
 * Feign不需要添加断路器依赖，Feign自身就带有断路器的会掉fallback
 */
@FeignClient(value="SERVICE-PROVIDER",fallback=HelloServiceImplHystrix.class)
public interface HelloService {
	
	@RequestMapping(value="/prvider",method=RequestMethod.GET)
	String hello(@RequestParam(value="name") String name);
	
}
