package com.jeegem.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
/*
 * @FeignClient(value="SERVICE-PROVIDER")
 * 这里同样需要大写
 */
@FeignClient(value="SERVICE-PROVIDER")
public interface HelloService {
	
	@RequestMapping(value="/prvider",method=RequestMethod.GET)
	String hello(@RequestParam(value="name") String name);
	
}
