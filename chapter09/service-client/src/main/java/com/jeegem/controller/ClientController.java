package com.jeegem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ClientController {
	
	@Autowired
	private RestTemplate restTemplate;
	
	@RequestMapping("/client")
	public String client() {
		return "client-zipkin";
	}
	
	@RequestMapping("/getProvider")
	public String getProvider() {
		return restTemplate.getForObject("http://localhost:8989/provider", String.class);
	}
	
}
