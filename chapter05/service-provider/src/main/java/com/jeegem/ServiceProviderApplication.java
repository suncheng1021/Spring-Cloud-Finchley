package com.jeegem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/*
 * 无论是服务提供方还是服务调用方
 * 相对于注册中心都是客户端
 */
@SpringBootApplication
@EnableEurekaClient
public class ServiceProviderApplication {
	public static void main(String[] args) {
		SpringApplication.run(ServiceProviderApplication.class, args);
	}
}
